.PHONY: all venv pypi ci.pypi test doc

PACKAGE_NAME:=$(shell python3 -c 'import setup; print(setup.PACKAGE_NAME)')
DIST:=$(shell python3 -m setup)
IDENTITY:=$(shell git config user.signingkey)
PYTEST:=$(shell if test -t 0; then echo "py.test --nf --ff --pdb --cov-report term-missing"; else echo "py.test"; fi)
TWINE:=twine upload --skip-existing

all: $(DIST)

venv: venv/activate
	@echo source $<

venv/activate: venv/$(PACKAGE_NAME)/bin/activate
	rm -f $@
	ln -s $(PACKAGE_NAME)/bin/activate $@

venv/$(PACKAGE_NAME)/bin/activate:
	rm -rf venv
	which virtualenv 2>/dev/null || pip install --user --upgrade virtualenv
	which virtualenv
	virtualenv --python=$$(which python3) venv/$(PACKAGE_NAME)

dist/%.whl:
	python setup.py bdist_wheel

dist/%.tar.gz:
	python setup.py sdist

pypi: $(DIST)
	@test -n "$(DIST)"
	@TWINE_USERNAME=neze \
		TWINE_PASSWORD="$$(pass www/pypi.org | head -n1 | tr -d '\n')" \
		$(TWINE) \
		--sign --sign-with gpg2 --identity $(IDENTITY) \
		$(DIST)

ci.pypi: $(DIST)
	@test -n "$(DIST)"
	@$(TWINE) $(DIST)

test:
	$(PYTEST) --cov=$(PACKAGE_NAME)

doc:
	python setup.py doc

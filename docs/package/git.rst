Git helpers
===========

The :py:mod:`pep440nz.git` provides helpers to read version
numbers in git tags.

.. automodule:: pep440nz.git
    :members:

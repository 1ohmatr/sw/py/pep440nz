Version numbers
===============

The :py:mod:`pep440nz.version` module provides classes corresponding
to the PEP440 syntax for version numbers.

.. automodule:: pep440nz.version
    :members:

import pytest,random

_v = ['','v','V']
def random_v():
    return _v[random.randrange(3)]

def random_epoch():
    if random.randrange(2):
        l = random.randrange(10)
        e = random.randrange(10**l)
        return '{:d}!'.format(e),e
    return '',0

def random_release():
    r = [random.randrange(50)]
    while random.randrange(4):
        r.append(random.randrange(100))
    return '.'.join(map(str,r)),r

# Pre-release
# [.-_]?{a|b|rc}([.-_]?N)?  If no number (last group), pre is implicit
_pre_val = [None,'a','b','rc']
_pre_label = {
    'a': ['a','alpha'],
    'b': ['b','beta'],
    'rc': ['rc','pre','preview','c']
}
_pre_sep = ['','.','-','_']
def random_pre():
    p = random.choice(_pre_val)
    if p is None:
        return '',None,False
    x = p
    p = random.choice(_pre_sep)+random.choice(_pre_label[p])
    y,i = 0,random.randrange(10)==0
    if not i:
        y = random.randrange(100)
        p+=random.choice(_pre_sep)+str(y)
    return p,(x,y),i

# Post-release
# [.-_]?{post|rev|r}?([.-_]?N)? If no number, post is implicit
# post cannot be implicit if 'post,rev,r' is not present
# if post is implicit, separator has to be '-'
_post_val = ['',None,'post','rev','r']
_post_sep = _pre_sep
def random_post(ipre):
    p = random.choice(_post_val[1*ipre:])
    if p is None:
        return '',None,False
    y,i = 0,p and (random.randrange(10)==0)
    if p:
        p = random.choice(_pre_sep)+p
        s = random.choice(_pre_sep)
    else:
        p = '-'
        s = ''
    if not i:
        y = random.randrange(100)
        p+= s+str(y)
    return p,y,i

# Dev-release
# [.-_]?dev([.-_]?N)?
# If no number, dev is implicit
_dev_val = [None,'dev']
_dev_sep = _pre_sep
def random_dev():
    d = random.choice(_dev_val)
    if d is None:
        return '',None,False
    d = random.choice(_pre_sep)+d
    y,i = 0,random.randrange(10)==0
    if not i:
        y = random.randrange(100)
        d+= random.choice(_pre_sep)+str(y)
    return d,y,i

# Local label
_local_sep = _pre_sep[1:]
def random_local():
    l = []
    s = []
    while random.randrange(3):
        if l:
            s.append(random.choice(_local_sep))
        else:
            s.append('+')
        l.append(hex(random.getrandbits(4*random.randrange(1,17)))[2:])
    return ''.join(map(lambda x: ''.join(x),zip(s,l))),l or None

class RandomVersionString(object):
    def __init__(self,seed):
        self._value = {}
        state = random.getstate()
        random.seed(seed)
        self.state = random.getstate()
        random.setstate(state)
        self.random()
    def keys(self):
        return ['str_v',
                'e','str_e',
                'r','str_r',
                'p','str_p',
                'P','str_P',
                'd','str_d',
                'l','str_l']
    def __getitem__(self,key):
        return self._value[key]
    def __setitem__(self,key,value):
        assert key in self.keys()
        self._value[key] = value
    def random(self):
        state = random.getstate()
        random.setstate(self.state)
        self['str_v']             = random_v()
        self['str_e'],self['e']   = random_epoch()
        self['str_r'],self['r']   = random_release()
        self['str_p'],self['p'],i = random_pre()
        self['str_P'],self['P'],i = random_post(i)
        self['str_d'],self['d'],i = random_dev()
        self['str_l'],self['l']   = random_local()
        random.setstate(state)
    def __str__(self):
        return ''.join(map(lambda x: self['str_'+x],"verpPdl"))
    def __repr__(self):
        res = ''
        if self['e']:
            res += str(self['e'])+'!'
        res += '.'.join(map(str,self['r']))
        if self['p'] is not None:
            res += ''.join(map(str,self['p']))
        if self['P'] is not None:
            res += '.post' + str(self['P'])
        if self['d'] is not None:
            res += '.dev' + str(self['d'])
        if self['l']:
            res += '+' + '.'.join(self['l'])
        return res

def random_list(seed,size):
    state = random.getstate()
    random.seed(seed)
    l = list(random.getrandbits(20) for _ in range(size))
    random.setstate(state)
    return l

@pytest.fixture(params=random_list(14759,100))
def seed(request):
    return request.param

@pytest.fixture(params=random_list(25467,20))
def integer(request):
    return request.param

@pytest.fixture(params=random_list(36148,20))
def integer_list(request):
    return random_list(request.param,1+(request.param % 20))

@pytest.fixture(params=random_list(14380,50))
def integer_fixed_list(request):
    def _generator(length):
        return random_list(request.param,length)
    return _generator

@pytest.fixture(params=random_list(29984,500))
def versionstring(request):
    """
    Generates many random version strings
    """
    return RandomVersionString(request.param)

@pytest.fixture(params=zip(random_list(8221,500),random_list(62027,500)))
def versionstringcouple(request):
    """
    Generates many random version strings
    """
    return tuple(map(RandomVersionString,request.param))

@pytest.fixture(params=random_list(10205,50))
def random_localrelease(request):
    state = random.getstate()
    random.seed(request.param)
    ret = random_local()
    random.setstate(state)
    return ret


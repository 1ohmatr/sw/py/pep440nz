import os,sys
sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))
import setup
from importlib import import_module
package = import_module(setup.PACKAGE_NAME)
__all__=['package']

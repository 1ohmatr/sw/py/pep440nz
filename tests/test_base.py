from context import package as p
import pytest

def test_min_value_version():
    assert p.Version.MIN == p.Version('0a0.dev0')

nif = [lambda : p.version._Member(None),
       lambda : p.version.Version.MIN.value]

@pytest.mark.parametrize('f',nif)
def test_notimplementedmethods(f):
    with pytest.raises(NotImplementedError):
        f()

def test_empty_version_init():
    with pytest.raises(ValueError):
        _ = p.version.Version()

def test_invalid_version_init():
    with pytest.raises(ValueError):
        _ = p.version.Version('this is not a valid version string')

@pytest.mark.parametrize('x',[list(),set(),tuple(),None])
def test_mistyped_version_init(x):
    with pytest.raises(TypeError):
        _ = p.version.Version(x)

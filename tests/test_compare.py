from context import package as p
import pytest,itertools,random
V = p.Version

v_lists = []
v_lists.append(['0.9',
                '0.9.1',
                '0.9.2',
                '0.9.10',
                '0.9.11',
                '1.0',
                '1.0.1',
                '1.1',
                '2.0',
                '2.0.1'])

v_lists.append(['3.14a12',
                '3.14b7',
                '3.14rc5',
                '3.14'])

v_lists.append(['2013.10',
                '2014.4',
                '1!1.0',
                '1!1.1',
                '1!2.0'])

v_lists.append(['0.1',
                '0.2',
                '0.3',
                '1.0',
                '1.1'])

v_lists.append(['1.1.0',
                '1.1.1',
                '1.1.2',
                '1.2.0'])

v_lists.append(['0.9',
                '1.0a1',
                '1.0a2',
                '1.0b1',
                '1.0rc1',
                '1.0',
                '1.1a1'])

v_lists.append(['0.9',
                '1.0.dev1',
                '1.0.dev2',
                '1.0.dev3',
                '1.0.dev4',
                '1.0rc1',
                '1.0rc2',
                '1.0',
                '1.0.post1',
                '1.1.dev1'])

v_lists.append(['1.0.dev456',
                '1.0a1',
                '1.0a2.dev456',
                '1.0a12.dev456',
                '1.0a12',
                '1.0b1.dev456',
                '1.0b2',
                '1.0b2.post345.dev456',
                '1.0b2.post345',
                '1.0rc1.dev456',
                '1.0rc1',
                '1.0',
                '1.0+5',
                '1.0+abc.5',
                '1.0+abc.7',
                '1.0.post456.dev34',
                '1.0.post456',
                '1.1.dev1'])

@pytest.mark.parametrize('strings',v_lists)
def test_sort(strings,seed,debug=lambda : None):
    random.seed(seed)
    original = list(map(V,strings))
    shuffled = original.copy()
    random.shuffle(shuffled)
    shuffled.sort()
    assert original == shuffled
    for x,y in zip(shuffled[:-1],shuffled[1:]):
        assert x<=y
        assert y>=x
        if x!=y:
            assert x<y
            assert y>x
    shuffled = list(map(str,shuffled))
    assert strings == shuffled

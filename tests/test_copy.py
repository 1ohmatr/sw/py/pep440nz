from context import package as p
V = p.Version

def test_copy(versionstring,debug = lambda : None):
    v0 = V(str(versionstring))
    v1 = V(epoch   = versionstring['e'],
           release = versionstring['r'],
           pre     = versionstring['p'],
           post    = versionstring['P'],
           dev     = versionstring['d'],
           local   = versionstring['l'])
    # Check that v1 was correctly built
    debug()
    assert v1 == v0
    assert str(v1) == repr(versionstring)
    v2 = v1.copy()
    # Check full copy
    debug()
    assert str(v2) == str(v1)
    assert dict(v2) == dict(v1)
    # Check attributes are distinct
    for attr in ['epoch','release','pre','post','dev','local']:
        assert id(getattr(v1,attr)) != id(getattr(v2,attr))

    # Deep copy without re-parsing
    v3 = V(epoch   = v1.epoch  .copy(),
           release = v1.release.copy(),
           pre     = v1.pre    .copy(),
           post    = v1.post   .copy(),
           dev     = v1.dev    .copy(),
           local   = v1.local  .copy())
    # Check full copy
    debug()
    assert str(v3) == str(v1)
    assert dict(v3) == dict(v1)
    # Check attributes are distinct
    for attr in ['epoch','release','pre','post','dev','local']:
        assert id(getattr(v1,attr)) != id(getattr(v2,attr))

if __name__=='__main__':
    import sys,pdb
    from conftest import RandomVersionString as RVS
    vs = RVS(int(sys.argv[1]))
    test_copy(vs,debug=pdb.set_trace)

from context import package as p
import itertools
import pytest

zero_epoch = [None,'','0','0!']
@pytest.mark.parametrize('e',zero_epoch)
def test_default_epoch(e):
    epoch = p.version.Epoch(e)
    assert epoch.value == 0
    assert str(epoch) == ''
    assert repr(epoch) == "Epoch('')"

mistyped_epoch = [dict(),list(),tuple(),bytes(),p.version.Epoch()]
@pytest.mark.parametrize('e',mistyped_epoch)
def test_typeerror_epoch(e):
    with pytest.raises(TypeError,match=str(type(e))):
        epoch = p.version.Epoch(e)

def test_valueerror_negative_epoch(integer):
    e = - integer
    if e == 0:
        e -= 1
    with pytest.raises(ValueError,match=r'^<0$'):
        epoch = p.version.Epoch(e)

badstring_epoch = ['0!!','!','x','f00bar','14632718?']
@pytest.mark.parametrize('e',badstring_epoch)
def test_valueerror_badstring_epoch(e):
    with pytest.raises(ValueError,match=e):
        epoch = p.version.Epoch(e)

mistyped_release = [dict(),tuple(),bytes(),p.version.Release('0'),None]
@pytest.mark.parametrize('r',mistyped_release)
def test_typeerror_release(r):
    with pytest.raises(TypeError,match=str(type(r))):
        release = p.version.Release(r)

def test_release_string(integer_list):
    release = p.version.Release(integer_list)
    assert str(release) == '.'.join(map(str,integer_list))

def test_valueerror_negative_release(integer_list):
    l = list(integer_list)
    l[l[0] % len(l)] *= -1
    with pytest.raises(ValueError,match=str(l)):
        release = p.version.Release(l)
    with pytest.raises(ValueError,match=str(l)):
        release = p.version.Release(str(l))
    with pytest.raises(ValueError):
        release = p.version.Release(-(integer_list[0]|1))

empty_release = ['',[]]
@pytest.mark.parametrize('r',empty_release)
def test_valueerror_empty_release(r):
    with pytest.raises(ValueError):
        release = p.version.Release(r)

def test_release_add_int(integer_list,integer):
    l = list(integer_list)
    r = p.version.Release(l)
    l.append(l.pop()+integer)
    assert (r+integer).value == l

def test_release_append_pop(integer_list,integer):
    r = p.version.Release(integer_list)
    r.append(integer)
    assert r.value == integer_list+[integer]
    r.pop()
    assert r.value == integer_list

def test_singleton_prerelease(integer):
    r = p.version.PreRelease((integer,))
    assert r.value == ('rc',integer)

rt = ['a','alpha','beta','c','pre','preview','b','rc']
def test_negative_prerelease(integer):
    with pytest.raises(ValueError,match=str(-integer)):
        r = p.version.PreRelease(-integer)
    with pytest.raises(ValueError,match=str(-integer)):
        r = p.version.PreRelease((-integer,))
    with pytest.raises(ValueError,match=str(-integer)):
        r = p.version.PreRelease((rt[integer % len(rt)],-integer))

def test_bad_tuple_prerelease(integer_list):
    l = list(integer_list)
    if len(l)==1:
        l.pop()
    elif len(l)==2:
        l.insert(0,'rc')
    with pytest.raises(ValueError):
        r = p.version.PreRelease(tuple(l))

mistyped_prerelease = [list(),dict(),set()]
@pytest.mark.parametrize('pr',mistyped_prerelease)
def test_mistyped_prerelease(pr):
    with pytest.raises(TypeError,match=str(type(pr))):
        _ = p.version.PreRelease(pr)

empty_prerelease = [None,'',(None,),(None,'x')]
@pytest.mark.parametrize('pr',empty_prerelease)
def test_empty_prerelease(pr):
    r = p.version.PreRelease('')
    assert r.value is None

def test_invalid_prerelease():
    with pytest.raises(ValueError):
        _ = p.version.PreRelease('-')

def test_prerelease_plus_int(integer_fixed_list):
    a,b,c = tuple(integer_fixed_list(3))
    pr0 = p.version.PreRelease((rt[a%len(rt)],b))
    pr1 = p.version.PreRelease((rt[a%len(rt)],b+c))
    assert (pr0+c) == pr1

def test_postrelease_init(integer):
    pr = []
    pr.append((integer,))
    pr.append(('-{:d}'.format(integer),))
    pr.append(('.post{:d}'.format(integer),))
    pr.append((str(integer),))
    pr.append((None,None,integer))
    pr.append(('r',integer,None))
    pr.append(('r',str(integer),None))
    assert all(type(x)==tuple for x in pr)
    pr = list(map(p.version.PostRelease,pr))
    assert all(x==y for x,y in itertools.product(pr,pr))

def test_bad_postrelease_init(integer):
    pr = [tuple()]
    pr.append((integer,integer))
    pr.append((integer,integer,integer,integer))
    pr.append(-integer)
    pr.append(str(integer))
    for x in pr:
        with pytest.raises(ValueError):
            _ = p.version.PostRelease(x)

@pytest.mark.parametrize('x',[list(),set(),dict()])
def test_mistyped_postrelease(x):
    with pytest.raises(TypeError,match=str(type(x))):
        _ = p.version.PostRelease(x)

def test_devrelease_init(integer):
    dr = [(integer,),
          ('.dev%d' % integer,),
          ('dev',integer),
          ('dev',str(integer)),
       ]
    assert all(type(x)==tuple for x in dr)
    dr+= [integer,'.dev%d' % integer]
    dr = list(map(p.version.DevRelease,dr))
    assert all(x==y for x,y in itertools.product(dr,dr))

def test_bad_devrelease_init(integer):
    pr = [tuple(),(integer,integer,integer),-integer,str(integer)]
    for x in pr:
        with pytest.raises(ValueError):
            _ = p.version.DevRelease(x)

@pytest.mark.parametrize('x',[list(),set(),dict()])
def test_mistyped_devrelease(x):
    with pytest.raises(TypeError,match=str(type(x))):
        _ = p.version.DevRelease(x)

@pytest.mark.parametrize('x',[list(),'+'])
def test_bad_localrelease_init(x):
    with pytest.raises(ValueError):
        _ = p.version.LocalRelease(x)

def test_localrelease_init(random_localrelease):
    s,l = random_localrelease
    if l is None:
        assert p.version.LocalRelease().value is l
        lr = [(l,),l,'']
    else:
        lr = [(list(l),),
            tuple(l),
            '+'+'.'.join(l),
            s]
    lr = list(map(p.version.LocalRelease,lr))
    assert all(x==y for x,y in itertools.product(lr,lr))
    assert all(x.value == l for x in lr)

@pytest.mark.parametrize('x',[set(),dict()])
def test_mistyped_localrelease(x):
    with pytest.raises(TypeError,match=str(type(x))):
        _ = p.version.LocalRelease(x)

from context import package as p
import random,pytest
V = p.Version

def test_add_int(versionstring):
    v0 = V(str(versionstring))
    delta = random.randrange(1,100)
    v1 = v0 + delta
    assert v1 > v0
    v2 = v0.copy()
    v2+= delta
    assert v1 == v2

def test_add(versionstringcouple):
    vA,vB = tuple(map(lambda x: V(str(x)),versionstringcouple))
    vC = vA.copy()

    # Add epoch
    v,a = vC.copy(),vB.epoch
    vC += a
    assert (a.value == 0 and v==vC) or v < vC

    # Add release
    v,a = vC.copy(),vB.release
    vC += a
    assert (all (x == 0 for x in a.value) and v==vC) or v < vC

    # Add pre-release
    v,a,b = vC.copy(),vB.pre,vA.pre
    if (a.value is None) or (b.value is None) or (a[0]==b[0]):
        vC += a
        assert ((a.value is None or a[1]==0) and v==vC) or \
               (b.value is None and v > vC) or             \
               v < vC
    else:
        with pytest.raises(ValueError):
            vC += a
        assert v==vC
        vB.pre = None

    # Add post-release
    v,a = vC.copy(),vB.post
    vC += a
    assert ((a.value is None or a.value == 0) and v==vC) or v < vC

    # Add dev-release
    v,a,b = vC.copy(),vB.dev,vA.dev
    vC += a
    assert ((a.value is None or (a.value == 0 and b.value is not None)) and v == vC) \
           or (b.value is None and v > vC) \
           or v < vC

    # Add local tag
    v,a = vC.copy(),vB.local
    vC += a
    assert ((a.value is None or a.value == []) and v == vC) or v < vC

    vE = vA.copy()
    vD = vA+vB
    vE += vB
    assert vD == vC and vD == vE
    vA.local = None
    vB.local = None
    assert (vA+vB) == (vB+vA)

from context import package as p
import pytest
V = p.Version

def test_parsing(versionstring,debug=lambda : None):
    version = V(str(versionstring))
    debug()
    assert version.epoch.value   == versionstring['e']
    assert version.release.value == versionstring['r']
    assert version.pre.value     == versionstring['p']
    assert version.post.value    == versionstring['P']
    assert version.dev.value     == versionstring['d']
    assert version.local.value   == versionstring['l']
    assert str(version)          == repr(versionstring)
    assert str(version)          == version
    v = V(V.MIN)
    v.value = version
    assert version == v
    with pytest.raises(KeyError):
        _ = v['randomkey']

def test_init(versionstring):
    version = V(str(versionstring))
    v0 = V(V.MIN)
    v1 = V(V.MIN)
    for k,v in {'e':'epoch','r':'release','p':'pre','P':'post','d':'dev','l':'local'}.items():
        v0[v] = versionstring[k]
        v1[v] = version[v]
    assert v0 == version
    assert v1 == version
    for v in ['epoch','pre','post','dev','local']:
        del v0[v]
        del v1[v]
    assert dict(v0) == dict(v1.items()) == {k: v for k,v in zip(v1.keys(),v1.values())}

if __name__=='__main__':
    import sys,pdb
    from conftest import RandomVersionString
    versionstring = RandomVersionString(int(sys.argv[1]))
    test_parsing(versionstring,debug=pdb.set_trace)

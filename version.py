from importlib import import_module
import os.path
import setup as ps
p = import_module(ps.PACKAGE_NAME)
v = p.head_tag_description().version()

vstring = v.str_version
fstring = str(v)
rstring = v.str_release

with open(os.path.join(ps.PACKAGE_NAME,'__version__.py'),'w') as f:
    f.write("VERSION='{}'\n".format(rstring))

if __name__=='__main__':
    print(fstring)
    print('version',vstring)
    print('release',rstring)
